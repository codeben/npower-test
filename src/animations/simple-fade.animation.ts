import { trigger, state, transition, style, animate } from '@angular/animations';

const simpleFade: Array<Object> = [
  trigger('simpleFadeAnimation', [
    state('in', style({opacity: 1})),
    transition(':enter', [
      style({opacity: 0}),
      animate(200)
    ]),
    transition(':leave',
      animate(200, style({opacity: 0})))
  ])
];

export default simpleFade;
