import { Component, HostListener } from '@angular/core';
import Block from 'src/interfaces/block.interface';
import data from 'src/data/block.data';
import { getBodyNode } from '@angular/animations/browser/src/render/shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'npower-test';
  data: Block[] = data;
  modalBlock: Block | null = null;
  lastElement: HTMLElement;

  focusLastElement(): void {
    if (this.lastElement && this.lastElement.focus) {
      this.lastElement.focus();
    }
  }

  buttonClick = (detailObject): void => {
    const { event, blockId }: {event: Event, blockId: number} = detailObject;
    event.preventDefault();
    this.modalBlock = Object.assign({}, this.data[blockId]);
  }

  modalClose = (event: Event): void => {
    event.preventDefault();
    if ((<HTMLElement> event.target).matches('.modal__close')
      || (<HTMLElement> event.target).matches('.modal__inner')) {
      this.modalBlock = null;
      this.focusLastElement();
    }
  }

  @HostListener('document:keydown', ['$event'])
  keyPressed = (event: KeyboardEvent): void => {
    if (this.modalBlock !== null) {
      // If is escape, cause close modal...
      if (event.keyCode === 27) {
        this.modalBlock = null;
        this.focusLastElement();
      }
      // If is tab, prevent tabbing...
      if (event.keyCode === 9) {
        event.preventDefault();
      }
    } else {
      this.lastElement = <HTMLElement>event.srcElement;
    }
  }

  @HostListener('document:mousedown', ['$event'])
  mouseDown = (event: MouseEvent): void => {
    if (!this.modalBlock) {
      this.lastElement = <HTMLElement>event.srcElement;
    }
  }
}
