import { Component, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import simpleAnimation from '../../animations/simple-fade.animation';
import Block from 'src/interfaces/block.interface';

@Component({
  selector: 'app-col-block',
  templateUrl: './col-block.component.html',
  styleUrls: ['./col-block.component.scss'],
  animations: simpleAnimation,
})
export class ColBlockComponent {
  @HostBinding('class.block') true;
  @HostBinding('tabindex') 0;
  @Input() block: Block;
  @Output() blockCtaClick: EventEmitter<object> = new EventEmitter;

  clickButton(event: MouseEvent, blockId: number) {
    this.blockCtaClick.emit({event, blockId});
  }
}
