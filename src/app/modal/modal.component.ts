import { Component, Input, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import Block from 'src/interfaces/block.interface';
import simpleAnimation from '../../animations/simple-fade.animation';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: simpleAnimation,
})
export class ModalComponent {
  @Input() modalBlock: Block;
  @Output() closeModal: EventEmitter<Event> = new EventEmitter();

  closeButton: ElementRef;

  closeModalClick = (event: Event) => {
    this.closeModal.emit(event);
  }

  @ViewChild('closeModal')
  set close(close: ElementRef) {
    this.closeButton = close;
    this.focusOnLoad();
  }

  focusOnLoad(): void {
    if (this.closeButton) {
      this.closeButton.nativeElement.focus();
    }
  }

  showModal(): boolean {
    if (this.modalBlock) {
      document.body.style.overflow = 'hidden';
      return true;
    }
    document.body.style.overflow = 'initial';
    return false;
  }
}
