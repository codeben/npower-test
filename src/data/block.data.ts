import Block from 'src/interfaces/block.interface';

const data: Block[] = [
  {
    id: 0,
    title: 'Some title 1',
    image: 'http://place-hoff.com/600/300/',
    // tslint:disable-next-line:max-line-length
    text: 'Etiam semper leo sit amet scelerisque efficitur. Nunc quis est ante. Vestibulum porta eros sit amet lectus dictum, quis cursus est tempus. Aliquam erat volutpat. In sit amet lacus eros. Aliquam erat volutpat. Nunc id rutrum dolor, accumsan aliquam urna. Ut vel pharetra purus. Curabitur nec lorem et purus lacinia semper rhoncus et neque. Donec ut nunc tincidunt, rutrum est ac, tincidunt erat.',
    button: 'Modal 1',
    alt: 'Some alt 1'
  },
  {
    id: 1,
    title: 'Some title 2',
    image: 'http://place-hoff.com/600/300/',
    text: 'Some text 2',
    button: 'Modal 2',
    alt: 'Some alt 2'
  },
  {
    id: 2,
    title: 'Some title 3',
    image: 'http://place-hoff.com/600/300/',
    text: 'Some text 3',
    button: 'Modal 3',
    alt: 'Some alt 3'
  },
];

export default data;
