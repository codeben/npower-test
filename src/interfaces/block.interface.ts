export default interface Block {
  id: number;
  title: string;
  image: string;
  text: string;
  button: string;
  alt: string;
}
